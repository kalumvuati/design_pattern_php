<?php

return [
    'mysql' => [
        'db_host' => getenv('MYSQL_DB_HOST'),
        'db_name' => getenv('MYSQL_DB_NAME'),
        'db_user' => getenv('MYSQL_DB_USER'),
        'db_port' => getenv('MYSQL_DB_PORT'),
        'db_password' => getenv('MYSQL_DB_PASS'),
        'db_charset' => 'utf8',
    ],
    'postgresql' => [
        'db_host' => getenv('POSTGRESQL_DB_HOST'),
        'db_name' => getenv('POSTGRESQL_DB_NAME'),
        'db_user' => getenv('POSTGRESQL_DB_USER'),
        'db_port' => getenv('POSTGRESQL_DB_PORT'),
        'db_password' => getenv('POSTGRESQL_DB_PASS'),
        'db_charset' => 'utf8',
    ]
];