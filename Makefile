PHONY: install

install:
	composer install

server:
	php -S localhost:8000 -t public

up:
	docker-compose up --build -d

down:
	docker-compose down

down_v:
	docker-compose down -v