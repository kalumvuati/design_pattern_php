<?php

namespace App;

use App\Factory\Database\Database;
use Dotenv\Dotenv;

class App
{
    /**
     * @var null
     */
    private static $_instance = null;

    /**
     * @var null
     */
    private $db;

    public string $title;

    /**
     * @var $sgdb database type (SGDB)
     */
    private $sgdb;

    public function __construct()
    {
        $this->title = "tutofree";
    }

    public static function getInstance(): App
    {
        if (is_null(static::$_instance)) {
            static::$_instance = new self();

            // Initiate DotEnv
            $dotenv = Dotenv::createUnsafeImmutable(dirname(__DIR__));
            $dotenv->load();

            // Display errors
            ini_set('display_errors', getenv('DEBUG'));

            // Defined Project dir name
            if (!defined('PROJECT_DIR')) {
                define("PROJECT_DIR", dirname(__DIR__));
            }
        }

        return static::$_instance;
    }

    // Factory to create Database instance by sgdb given
    public function getDatabase(string $sgdb): Database
    {
        $_instance_name = 'App\\Factory\\Database\\' . ucfirst($sgdb) . 'Database';
        $this->sgdb = $sgdb;

        return $_instance_name::getInstance();
    }

    public function getTable(string $name) {

        $table = 'App\\Factory\\Table\\' .ucfirst($name) . 'Table';

        return new $table($this->getDatabase($this->sgdb));
    }
}
