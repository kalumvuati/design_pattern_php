<?php

namespace App\Fluent;

class QueryBuilder
{

    /**
     * @var string $columns
     */
    private $columns;

    /**
     * @var array $table
     */
    private array $table = [];

    /**
     * @return $this
     */
    public function select() : self
    {
        $this->columns = func_get_args();

        return $this;
    }

    public function from(string $table, string $alias = "") : self
    {
        if (empty($alias)) {
            $this->table[] = $table;
        } else {
            $this->table[] = $table . ' AS ' . $alias;
        }

        return $this;
    }

    public function where() : self
    {
        foreach (func_get_args() as $condition) {
            $this->where[] = $condition;
        }

        return $this;
    }

    /**
     * @return string
     */
    public function __toString() : string
    {
        return 'SELECT ' . implode(', ', $this->columns) . ' FROM ' . implode(', ', $this->table) . ' WHERE ' . implode(' AND ', $this->where);
    }
}
