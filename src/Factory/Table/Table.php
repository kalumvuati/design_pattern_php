<?php

namespace App\Factory\Table;

use App\Factory\Database\Database;

class Table implements TableInterface
{

    /**
     * @var string table name
     */
    protected string $name = "";
    private \PDO $db;

    public function __construct(Database $database)
    {
        if ($this->name === "") {
            $explode = explode('\\', get_class($this));
            $this->name = strtolower(str_replace('Table', '', end($explode)));
            $this->db = $database->getDb();
        }
    }

    public function finAll(array $options = [], string $classname = ''): mixed
    {
        try {
            $stmt = $this->db->prepare("SELECT * FROM {$this->name}");
            $stmt->execute();

            return $stmt->fetchAll(\PDO::FETCH_OBJ);
        } catch (\PDOException $e) {
            return [];
        }
    }

    public function find(string $id, string $classname = ''): mixed
    {
        try {
            $stmt = $this->db->prepare("SELECT * FROM {$this->name} WHERE id = :id");
            $stmt->execute(['id' => $id]);

            return $stmt->fetch(\PDO::FETCH_OBJ);
        } catch (\PDOException $e) {
            return [];
        }
    }

    public function finOneBy(array $options = [], string $classname = ''): mixed
    {
        try {
            $stmt = $this->db->prepare("SELECT * FROM {$this->name} WHERE {$options['key']} = :{$options['key']}");
            $stmt->execute([$options['key'] => $options['value']]);

            return $stmt->fetch(\PDO::FETCH_OBJ);
        } catch (\PDOException $e) {
            return [];
        }
    }

    public function findByRequest(string $statement): mixed
    {
        try {
            $stmt = $this->db->prepare($statement);
            $stmt->execute();

            return $stmt->fetchAll(\PDO::FETCH_OBJ);
        } catch (\PDOException $e) {
            return [];
        }
    }
}
