<?php

namespace App\Factory\Table;

interface TableInterface
{
    /**
     * @param array $options
     * @param string $classname
     * @return mixed
     */
    public function finAll(array $options = [], string $classname = ''): mixed;

    /**
     * @param string $id
     * @param string $classname
     * @return mixed
     */
    public function find(string $id, string $classname = ''): mixed;

    /**
     * @param array $options
     * @param string $classname
     * @return mixed
     */
    public function finOneBy(array $options = [], string $classname = ''): mixed;
}