<?php

namespace App\Factory\Database;

class Database
{

    /**
     * @var null
     */
    protected $db = null;

    /**
     * @var array|mixed
     */
    protected array $config;

    public function __get(string $name)
    {
        if (!isset($this->config[$name])) {
            return null;
        }

        return $this->config[$name];
    }

    public function __set(string $name, $value): void
    {
        $this->config[$name] = $value;
    }

    public function getDb() {
        return $this->db;
    }
}
