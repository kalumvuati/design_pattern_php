<?php

namespace App\Factory\Database;

use PDO;

class MysqlDatabase extends Database
{
    /**
     * @var null
     */
    protected static $_instance = null;

    public function __construct()
    {
        $config = require(PROJECT_DIR . '/config/config.php');
        $config = $config['mysql'] ;
        $this->config = $config;

        // Database init
        try {
            $this->db = new PDO(
                'mysql:host='.$this->config['db_host'].';port='.$this->config['db_port'].';dbname='.$this->config['db_name'],
                $this->config['db_user'],
                $this->config['db_password']
            );
            $this->db->exec("SET CHARACTER SET utf8");
        } catch (\PDOException $exception) {
            throw new \PDOException($exception->getMessage());
        }
    }

    public static function getInstance()
    {
        if (is_null(static::$_instance)) {
            static::$_instance = new self();
        }
        
        return static::$_instance;
    }

    public function __get(string $name)
    {
        if (!isset($this->config[$name])) {
            return null;
        }

        return $this->config[$name];
    }

    public function __set(string $name, $value): void
    {
        $this->config[$name] = $value;
    }
}
