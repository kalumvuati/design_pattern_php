<?php

namespace App\Factory\Database;

use PDO;

class PostgresqlDatabase extends Database
{
    /**
     * @var null
     */
    protected static $_instance = null;

    public function __construct()
    {
        $config = require(PROJECT_DIR . '/config/config.php');
        $config = $config['postgresql'] ;
        $this->config = $config;

        // Database init
        try {
            $dsn = "pgsql:host=".$config['db_host'].";port=".$config['db_port'].";dbname=".$config['db_name'];
            $this->db = new PDO($dsn, $config['db_user'], $config['db_password']);
        } catch (\PDOException $exception) {
            throw new \PDOException($exception->getMessage());
        }
    }

    public static function getInstance()
    {
        if (is_null(static::$_instance)) {
            static::$_instance = new self();
        }
        
        return static::$_instance;
    }
}
