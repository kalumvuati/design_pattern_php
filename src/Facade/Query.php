<?php

namespace App\Facade;

use App\Fluent\QueryBuilder;

// Facade Marche bien avec les méthodes statiques et Fluent
// ça cache la complexité de la classe QueryBuilder
class Query
{
    public static function __callStatic($method, $arguments)
    {
        /*
            $quety = call_user_func_array([new QueryBuilder(), $method], $arguments);
            return $quety;
        */
        return (new QueryBuilder())->$method(...$arguments);
    }
}
