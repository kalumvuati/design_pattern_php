<?php

use App\App;
use App\Facade\Query;
use App\Fluent\QueryBuilder;

require_once __DIR__ . '/../vendor/autoload.php';

// Initial App
$app = App::getInstance();

// Initial Mysql
$app->getDatabase('mysql');
$user_table = $app->getTable('users');
dump($user_table->finAll());
dump($user_table->find(4));
dump($user_table->finOneBy(['key' => 'name', 'value' => 'john']));

//$query = new QueryBuilder();
//dump($user_table->findByRequest($query
//    ->select('id', 'name', 'lastname', 'edited_at')
//    ->from('users')
//    ->where('edited_at  < now()')
//    ->where('id = 1', 'lastname = "doe"')));

$resultat = $user_table->findByRequest(
    Query::select('id', 'name', 'lastname', 'edited_at')
    ->from('users')
    ->where('edited_at  < now()')
    ->where('lastname = "toto"')
);

dd($resultat);


